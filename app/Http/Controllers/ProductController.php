<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Support\Str;
use File;
use App\Jobs\ProductJob;

class ProductController extends Controller
{
    public function index() {
        $products = Product::with(["category"])->orderBy("created_at", "DESC");

        if (request()->q != "") {
            $products = $products->where("name", "LIKE", "%" . request()->q . "%");
        }

        $products = $products->paginate(10);

        return view("products.index", compact("products"));
    }

    public function create() {
        $categories = Category::orderBy("name", "ASC")->get();

        return view("products.create", compact("categories"));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'required|image|mimes:png,jpeg,jpg'
        ]);

        if ($request->hasFile("image")) {
            $file = $request->file("image");
            $filename = time() . "-" . Str::slug($request->name) . "." . $file->getClientOriginalExtension();
            $file->storeAs("public/products", $filename);

            $product = Product::create([
                'name' => $request->name,
                'slug' => $request->name,
                'category_id' => $request->category_id,
                'description' => $request->description,
                'image' => $filename,
                'price' => $request->price,
                'weight' => $request->weight,
                'status' => $request->status
            ]);

            return redirect(route('product.index'))->with(['success' => 'Produk berhasil ditambahkan.']);
        }
    }

    public function edit($id) {
        $product = Product::findOrFail($id);
        $categories = Category::orderBy("name", "DESC")->get();

        return view("products.edit", compact("product", "categories"));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'description' => 'required',
            'category_id' => 'required|exists:categories,id',
            'price' => 'required|integer',
            'weight' => 'required|integer',
            'image' => 'nullable|image|mimes:png,jpeg,jpg'
        ]);

        $product = Product::findOrFail($id);
        $filename = $product->image;

        if ($request->hasFile("image")) {
            $file = $request->file("image");
            $filename = time() . "-" . Str::slug($request->name) . "." . $file->getClientOriginalExtension();

            $file->storeAs("public/products", $filename);
            
            File::delete(storage_path('app/public/products/' . $product->image));
        }

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'weight' => $request->weight,
            'image' => $filename
        ]);
        
        return redirect(route('product.index'))->with(['success' => 'Data produk diperbaharui.']);
    }

    public function destroy($id) {
        $product = Product::findOrFail($id);

        File::delete(storage_path("app/public/products/") . $product->image);

        $product->delete();

        return redirect(route('product.index'))->with(['success' => 'Produk berhasil dihapus.']);
    }

    public function massUploadForm() {
        $categories = Category::orderBy("name", "DESC")->get();

        return view("products.bulk", compact("categories"));
    }

    public function massUpload(Request $request) {
        $this->validate($request, [
            "category_id" => "required|exists:categories,id",
            'file' => 'required|mimes:xlsx'
        ]);

        if ($request->hasFile("file")) {
            $file = $request->file("file");
            $filename = time() . "-product." . $file->getClientOriginalExtension();
            $file->storeAs("public/uploads", $filename); 

            ProductJob::dispatch($request->category_id, $filename);

            return redirect()->back()->with(['success' => 'Upload produk dijadwalkan.']);
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index() {
        // Eager loading.
        // Method 'parent' merupakan sebuah fungsi relationship antar table.

        $categories = Category::with(["parent"])->orderBy("created_at", "DESC")->paginate(10);
        
        // Legal scope.
        // Method 'getParent()' akan mengembalikan parents nya saja.

        $parents = Category::getParent()->orderBy("name", "ASC")->get();

        return view("categories.index", compact("categories", "parents"));
    }

    public function store(Request $request) {
        $this->validate($request, [
            "name" => "required|string|max:50|unique:categories"
        ]);

        $request->request->add(["slug" => $request->name]);

        Category::create($request->except("_token"));

        return redirect(route("category.index"))->with(["success" => "Kategori berhasil ditambahkan."]);
    }

    public function edit($id) {
        $category = Category::findOrFail($id);
        $parents = Category::getParent()->orderBy("name", "ASC")->get();

        return view('categories.edit', compact('category', 'parents'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            "name" => "required|string|max:50|unique:categories,name," . $id
        ]);

        $category = Category::findOrFail($id);

        $category->update([
            "name" => $request->name,
            "parent_id" => $request->parent_id
        ]);

        return redirect(route("category.index"))->with(["success" => "Kategori berhasil diperbarui."]);
    }

    public function destroy($id) {
        // Eager loading.
        // Hasil querynya akan menambahkan field baru bernama child_count dan product_count. 

        $category = Category::withCount(["child", "product"])->find($id);

        if ($category->child_count == 0 && $category->product_count == 0) {
            $category->delete();
            return redirect(route("category.index"))->with(["success" => "Kategori berhasil dihapus."]);
        }

        return redirect(route("category.index"))->with(["error" => "Kategori sedang digunakan!."]);
    }
}
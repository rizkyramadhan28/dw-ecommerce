<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    protected $fillable = ["name", "parent_id", "slug"];

    // Relationship ke class induk.

    public function parent() {
        return $this->belongsTo(Category::class);
    }

    // Penamaan local scope harus diawali dengan 'scope' dan diikuti nama method yang diinginkan.

    public function scopeGetParent($query) {
        return $this->whereNull("parent_id");
    }

    // Mutator.

    public function setSlugAttribute($value) {
        $this->attributes["slug"] = Str::slug($value);
    }

    // Accessor.

    public function getNameAttribute($value) {
        return ucfirst($value);
    }

    // Relationship one to many dengan foreign key parent_id.

    public function child() {
        return $this->hasMany(Category::class, "parent_id");
    }

    public function product() {
        return $this->hasMany(Product::class);
    }
}